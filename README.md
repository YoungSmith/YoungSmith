---
title: 编写指南
---
下面是比较官方的一段话：

Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## 第一步：环境
- 安装Git —— [国内 Git 链接](https://github.com/waylau/git-for-win)
- 安装Node.js —— 建议下载绿色版，然后配置path即可。
- 安装Hexo 
```
npm install -g hexo-cli
```
## 第二步：下载代码
[地址](https://gitee.com/YoungSmith/YoungSmith.git)

## 第三步：下载Next主题
因为主题没有放到源码中，所以要另外下载。
在更目录执行
```
git clone https://github.com/theme-next/hexo-theme-next themes/next
```
## 第四步：编写文档
下面又是官方的一些说明。
### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)
